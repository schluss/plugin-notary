library notary;

import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';

import 'ui/processing_page.dart';

class PluginNotary {
  final StreamController<int> _streamController = StreamController.broadcast();

  void runPlugin(context, Function callBack, String siteUrl, String trigger, {String pluginName = 'Notaris'}) async {
    await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ProcessingPage(siteUrl),
      ),
    ).then((value) {
      // fire the callback function to send the data back to the app
      callBack(
        'notary',
        value, // json string
        context,
      );
    });
  }

  /// Gets extracted data
  Future<Map<String, dynamic>> getExtractedData(String jsonData) async {
    _streamController.add(50);
    Map<String, dynamic>? attributes = jsonDecode(jsonData);
    _streamController.add(100);
    return Future.value(attributes);
  }

  Stream<int> getProgress() {
    return _streamController.stream;
  }
}
